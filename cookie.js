/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD.Storage');

// Creates the namepsace for the object
CRD.Storage.Cookie = (function() {
	"use strict";
	
	/**
	 * Cookie Storage utilities object
	 * @type {{parsed: boolean, cookies: Object, setData: Cookie.getCookie, get: Cookie.get, set: Cookie.set}}
	 */
	
	var Cookie = {
		
		/**
		 * Cookies parsed?
		 * @property {Boolean} parsed - Cookies parsed?
		 */
		
		parsed : false,
		
		/**
		 * Cookies object collection
		 * @property {Object} cookies - Cookies collection
		 */
		
		cookies : {},
		
		/**
		 * Get all document cookies
		 * @return {Object}
		 */
		
		getCookies : function() {
			
			if(Cookie.parsed === false) {
				
				// Get all document cookies
				Cookie.cookies = decodeURIComponent(document.cookie).split(';');
				
				// Set the cookies as parsed
				Cookie.parsed = true;
				
			}
			
			// Return the cookies
			return Cookie.cookies;
			
		},
		
		/**
		 * Get cookies by name
		 * @param {String} name - Cookie name
		 * @return {String|null}
		 */
		
		get : function(name) {
			
			// Variables
			var name = name + '=',
				cookies = Cookie.getCookies(),
				cookie;
			
			// For each cookie
			for(var x = 0, max = cookies.length; x < max; x = x + 1) {
				
				// Gets the current cookie
				cookie = cookies[x];
				
				// Cleanup
				while(cookie.charAt(0) == ' ') {
					cookie = cookie.substring(1);
				}
				
				// If the current cookie is the requested one
				if (cookie.indexOf(name) == 0) {
					
					// Returns the cookie value
					return cookie.substring(name.length, cookie.length);
					
				}
				
			}
			
			// ):
			return null;
			
		},
		
		/**
		 * Set cookies
		 * @param {String} name    - Cookie name
		 * @param {String} value   - Cookie value
		 * @param {Number} expires - Days to expire, default to 30 days
		 * @param {String} path    - Valid path, default to '/'
		 */
		
		set : function (name, value, expires, path) {
			
			// Variables
			var date = new Date();
			
			// Normalizes the path
			expires = typeof expires === 'undefined' ? 30 : expires;
			
			// Set the expiration date
			date.setTime(date.getTime() + (expires * 24 * 60 * 60 * 1000));
			
			// Normalizes the path
			path = typeof path === 'undefined' ? '/' : path;
			
			// Sets the cookie
			document.cookie = name + '=' + value +
				';expires=' + date.toUTCString() +
				';path=' + path;
			
		}
		
	};
	
	// Returns the storage obejcts
	return Cookie;
	
})();