/* globals namespace, CRD */

/**
 * CRD namespace definition
 * @namespace
 */

namespace('CRD.Storage');

// Creates the namepsace for the object
CRD.Storage.Session = (function() {
	"use strict";
	
	/**
	 * Session Storage utilities object
	 * @type {{available: boolean, setData: Session.setData, getData: Session.getData, removeData: Session.removeData}}
	 */
	
	var Session = {
		
		/**
		 * Session Storage availability
		 * @property {Boolean} available - Session storage available?
		 */
		
		available : typeof sessionStorage !== 'undefined',
		
		/**
		 * Stores data in SessionStorage
		 * @method setData
		 * @param {String} key   - Key for the data
		 * @param {*}      value - Data to store
		 * @return {Session}
		 * @memberOf Session
		 * @public
		 */
		
		setData : function(key, value) {
			
			// If session storage is available
			if(Session.available) {
				
				// Store data in session storage
				sessionStorage.setItem(key, value);
				
			}
			
			// (:
			return Session;
			
		},
		
		/**
		 * Get stored data from SessionStorage
		 * @method getData
		 * @param {String} key          - Data key to retrieve
		 * @param {*}      defaultValue - Data key to retrieve
		 * @return {*}
		 * @memberOf Session
		 * @public
		 */
		
		getData : function(key, defaultValue) {
			
			// Variables
			var value = defaultValue || null;
			
			// If session storage is available
			if(Session.available) {
				
				// Value
				value = sessionStorage.getItem(key);
				
			}
			
			// Returns the value
			return value;
			
		},
		
		/**
		 * Removes data from session storage
		 * @method removeData
		 * @param {String} key - Data key to remove from SessionStorage (accepts multiple arguments as keys)
		 * @return {Session}
		 * @memberOf Session
		 * @public
		 */
		
		removeData : function(key) {
			
			// If session storage is available
			if(Session.available) {
				
				// for each defined argument
				for (var i = 0; i < arguments.length; i++) {
					
					// Removes data from session storare
					sessionStorage.removeItem(arguments[i]);
					
				}
				
			}
			
			// (:
			return Session;
			
		}
		
	};
	
	// Returns the storage obejcts
	return Session;
	
})();